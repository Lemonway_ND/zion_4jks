﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ForJenkinsTry.Startup))]
namespace ForJenkinsTry
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
